package pl.edu.uwm.wmii.derenwiktor.labolatorium02;

public class Zad2d {
    public static int sumaDodatnich(int tab[]) {
        int sumaDodatnich = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > 0) {
                sumaDodatnich+=tab[i];
            }
        }
        return sumaDodatnich;
    }

    public static int sumaUjemnych(int tab[]) {
        int sumaUjemnych = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < 0) {
                sumaUjemnych+=tab[i];
            }
        }
        return sumaUjemnych;
    }

    public static void main(String[] args) {
        int z[] = new int[5];
        Generuj.wpisz(z, 5, 0, 50);
        System.out.println("Suma liczb dodatnich: " + sumaDodatnich(z));
        System.out.println("Suma liczb ujemnych: " + sumaUjemnych(z));
    }
}