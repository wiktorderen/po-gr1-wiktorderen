package pl.edu.uwm.wmii.derenwiktor.labolatorium02;

public class Zad2b {
    public static int ileDodatnich(int tab[]) {
        int Dodatnie = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > 0) {
                Dodatnie++;
            }
        }
        return Dodatnie;
    }

    public static int ileUjemnych(int tab[]) {
        int Ujemne = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < 0) {
                Ujemne++;
            }
        }
        return Ujemne;
    }

    public static int ileZer(int tab[]) {
        int Zera = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == 0) {
                Zera++;
            }
        }
        return Zera;
    }
    public static void main(String[] args) {
        int z[] = new int[10];
        Generuj.wpisz(z, 10, 5, 50);
        System.out.println("Ilosc liczb dodatnich: " + ileDodatnich(z));
        System.out.println("Ilosc liczb ujemnych: " + ileUjemnych(z));
        System.out.println("Ilosc liczb zerowych: " + ileZer(z));

    }
}
