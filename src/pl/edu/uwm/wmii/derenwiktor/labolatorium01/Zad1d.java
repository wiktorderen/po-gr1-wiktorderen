package pl.edu.uwm.wmii.wiktorderen.laboratorium01;

import java.util.Scanner;
import java.lang.Math;

public class Zad1d {
    public static void main(String[] args) {
        int suma =0;
        int n=0;
        Scanner sc =new Scanner(System.in);
        System.out.print("Podaj liczbe elemntow: ");
        n=sc.nextInt();

        int i=0;

        while(i<n)
        {
            System.out.println("Podaj element" + (i+1) + ": ");

            double x= sc.nextInt();
            if(x<0)
            {
                x*=-1;
            }
            x=Math.sqrt(x);
            suma+=x;
            i++;
        }

        System.out.println("Suma elementów: " + suma);
    }
}
