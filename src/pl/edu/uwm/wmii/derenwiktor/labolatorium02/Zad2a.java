package pl.edu.uwm.wmii.derenwiktor.labolatorium02;

public class Zad2a {
    public static int ileParzystych(int tab[]) {
        int Parzyste = 0;
        for (int i = 0; i < tab.length; i++) {
            if ((tab[i] % 2) == 0) {
                Parzyste++;
            }
        }
        return Parzyste;
    }

    public static int ileNieparzystch(int tab[])

    {
        int Nieparzyste = 0;
        for (int i = 0; i < tab.length; i++) {
            if ((tab[i] % 2) != 0) {
                Nieparzyste++;
            }
        }
        return Nieparzyste;
    }
    public static void main(String[] args) {
        int z[]=new int[5];
        Generuj.wpisz(z, 5, 5, 50);
        System.out.println("Ilosc liczb parzystych: "+ileParzystych(z));
        System.out.println("Ilosc liczb nieparzystych: "+ileNieparzystch(z));

    }
}