package pl.edu.uwm.wmii.derenwiktor.labolatorium03;

import java.util.Scanner;

public class Zad1b04 {
    public static int countSubStr(String str, String subStr){
        int licznik=0;
        char lista[]=new char[subStr.length()];
        for(int i=0;i<str.length();i++)
        {
            if(str==str.concat(subStr))
            {
                lista[i]=subStr.charAt(i);
            }
        }
        return lista.length;
    }

    public static void main(String[] args) {
        String napis;
        String subnapis;
        Scanner odczyt=new Scanner(System.in);
        System.out.print("Podaj jakiś napis: ");
        napis = odczyt.nextLine();
        System.out.print("Podaj subnapis: ");
        subnapis=odczyt.nextLine();
        System.out.println("Liczba wystapień subnapisu "+subnapis+" w napisie "+napis+" to : "+countSubStr(napis, subnapis));

    }
}