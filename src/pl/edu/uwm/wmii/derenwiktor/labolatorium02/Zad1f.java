package pl.edu.uwm.wmii.derenwiktor.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1f {public static void main(String[] args) {
    int n;
    Scanner sc = new Scanner(System.in);
    System.out.print("Podaj liczbe: ");
    n = sc.nextInt();
    Random r = new Random();

    int[] tablica = new int[n];
    for (int i = 0; i < tablica.length; i++)
    {
        tablica[i] = r.nextInt() % 1000;
    }
    int sumaUjemne=0;
    int sumaDodatnie=0;
    for (int i = 0; i < tablica.length; i++)
    {
        System.out.println(tablica[i]);
        if(tablica[i]<0)
        {
            tablica[i]=-1;
        }
        if(tablica[i]>0)
        {
            tablica[i]=1;
        }
    }
    System.out.println("Tablica po zmianie: ");
    for(int i=0;i<tablica.length;i++)
    {
        System.out.println(tablica[i]);
    }
}
}
