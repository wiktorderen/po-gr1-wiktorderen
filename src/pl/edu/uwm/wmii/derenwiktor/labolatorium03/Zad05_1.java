package pl.edu.uwm.wmii.derenwiktor.labolatorium03;
import java.util.ArrayList;

public  class Zad05_1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer>b)
    {
        ArrayList<Integer>lista=new ArrayList<Integer>();
        lista.addAll(a);
        lista.addAll(b);
        System.out.print(lista+" ");
        return lista;
    }
    public static void main(String[] args) {
        ArrayList<Integer> AA=new ArrayList<>();
        AA.add(1);
        AA.add(4);
        AA.add(9);
        AA.add(16);
        ArrayList<Integer> BB=new ArrayList<>();
        BB.add(9);
        BB.add(7);
        BB.add(4);
        BB.add(9);
        BB.add(11);
        append(AA, BB);
    }
}
