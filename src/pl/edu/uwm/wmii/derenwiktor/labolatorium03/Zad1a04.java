package pl.edu.uwm.wmii.derenwiktor.labolatorium03;
import java.util.Scanner;
import java.lang.String;
public class Zad1a04 {
    public static int countChar(String napis, char litera){
        int licznik=0;
        char znak;
        for(int i=0;i<napis.length();i++)
        {
            znak=napis.charAt(i);
            if(znak==litera) licznik++;
        }
        return licznik;
    }
    public static void main(String[] args) {
        String napis;
        char litera;
        Scanner odczyt=new Scanner(System.in);
        System.out.print("Podaj jakiś napis: ");
        napis = odczyt.nextLine();
        System.out.print("Podaj litere: ");
        litera=odczyt.next().charAt(0);
        System.out.println("Liczba wystapień litery "+litera+" to : "+countChar(napis, litera));
    }
}