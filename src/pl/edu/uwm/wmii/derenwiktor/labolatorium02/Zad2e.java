package pl.edu.uwm.wmii.derenwiktor.labolatorium02;

public class Zad2e {
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]) {
        int dlugosc = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > 0) {
                dlugosc++;
            }
        }
        return dlugosc;
    }

    public static void main(String[] args) {
        int z[]=new int[5];
        Generuj.wpisz(z,5,0,50);
        System.out.println("Dlugosc makylamnego ciagu liczb dodatnich: "+dlugoscMaksymalnegoCiaguDodatnich(z));
    }
}
