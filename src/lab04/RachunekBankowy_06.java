package lab04;

public class RachunekBankowy_06
{
    static double rocznaStopaProcentowa;
    private float saldo;

    public RachunekBankowy_06(float saldo){
        this.saldo=saldo;
    }

    public void obliczMiesieczneOdsetki()
    {
        double miesieczneOdsetki=(saldo*rocznaStopaProcentowa)/12;
        saldo+=miesieczneOdsetki;
    }

    public static void setRocznaStopaProcentowa(double stopaProc)
    {
        rocznaStopaProcentowa=stopaProc;
    }

    public float getSaldo()
    {
        return saldo;
    }
}
//np do zliczania obiwektow ile ma obiektow uzywamy statycznej klasy