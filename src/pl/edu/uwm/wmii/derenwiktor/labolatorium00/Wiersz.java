package pl.edu.uwm.wmii.wiktorderen.laboratorium00;

public class Wiersz {
    public static void main(String[] args) {
        System.out.print("Ja tarsjusz syn tarsjusza, \n" +
                "wnuk tarsjusza i prawnuk, \n" +
                "zwierzątko małe, złożone z dwóch źrenic \n" +
                "i tylko bardzo już koniecznej reszty; \n" +
                "cudownie ocalony od dalszej przeróbki, \n" +
                "bo przysmak ze mnie żaden, \n" +
                "na kołnierz są więksi, \n" +
                "gruczoły moje nie przynoszą szczęścia, \n" +
                "koncerty odbywają się bez moichh jelit; \n" +
                "ja tarsjusz \n" +
                "siedzę żywy na palcu człowieka. \n" +
                "\n" +
                "Dzień dobry, wielki panie, \n" +
                "co mi za to dasz, \n" +
                "że mi niczego nie musisz odbierać? \n" +
                "Swoją wspaniałomyślność czym mi wynagrodzisz? \n" +
                "Jaką mi, bezcennemu, przyznasz cenę \n" +
                "za pozowanie do twoich uśmiechów? \n" +
                "Wielki pan dobry - \n" +
                "wielki pan łaskawy - \n" +
                "któż by mógł o tym świadczyć, gdyby brakło \n" +
                "zwierzątko małe, prawie jak półczegoś, \n" +
                "co jednak jest całością od innych nie gorszą; \n" +
                "tak lekki, że gałązki wznoszą się pode mną \n" +
                "i mogłyby mnie dawno w niebo wziąć, \n" +
                "gdybym niemusiał raz po raz \n" +
                "spadać kamieniem z serc \n" +
                "ach, roztkliwionych; \n" +
                "ja tarsjusz \n" +
                "wiem, jak bardzo trzeba być tarsjuszem. ");
    }
}