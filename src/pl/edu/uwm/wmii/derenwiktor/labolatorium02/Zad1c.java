package pl.edu.uwm.wmii.derenwiktor.labolatorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1c {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj liczbe: ");
        n = sc.nextInt();
        Random r = new Random();

        int[] tablica = new int[n];
        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = r.nextInt() % 1000;
        }
        int max = tablica[0];
        int licznik=0;
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);

            if (tablica[i] > max)
            {
                max = tablica[i];
                licznik++;
            }
        }
        System.out.println("Najwieksza liczba w tablicy to: "+max);
        System.out.println("Wystapila ona "+licznik+" razy");
    }
}
