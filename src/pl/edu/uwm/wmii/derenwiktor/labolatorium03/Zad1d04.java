package pl.edu.uwm.wmii.derenwiktor.labolatorium03;

import java.util.Scanner;

public class Zad1d04 {
    public static int StringRepeat(String str, int n){
        for(int i=0;i<n;i++)
        {
            System.out.print(str);
        }
        System.out.println(" ");
        return 0;
    }
    public static void main(String[] args) {
        String napis;
        int liczba;
        Scanner odczyt=new Scanner(System.in);
        System.out.print("Podaj jakiś napis: ");
        napis = odczyt.nextLine();
        System.out.print("Podaj cyfre/liczbe: ");
        liczba=odczyt.nextInt();
        System.out.println(StringRepeat(napis, liczba));
    }
}
