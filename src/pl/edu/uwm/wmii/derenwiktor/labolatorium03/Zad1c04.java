package pl.edu.uwm.wmii.derenwiktor.labolatorium03;
import java.lang.String;
import java.util.Scanner;

public class Zad1c04 {
public static String middle(String str){
    int dlugosc=str.length();

    if(dlugosc%2==0)
    {
        return str.substring(dlugosc / 2);
    }
    else
    {
        return str.substring(dlugosc/2,2);
    }
}
    public static void main(String[] args) {
            String napis;
            Scanner odczyt=new Scanner(System.in);
            System.out.print("Podaj jakiś napis: ");
            napis = odczyt.nextLine();
            System.out.println("Środkowy znak w napisie "+napis+" to : "+middle(napis));

        }
    }
